Listing Program 9.1
<br><br>

<?php
$gaji = 1000000;
$pajak = 0.1;
$thp = $gaji - ($gaji*$pajak);

echo "gaji sebelum pajak =  Rp. $gaji <br>";
echo "gaji yang dibawa pulang =  Rp . $thp";
?>

<br><br>
Listing Program 9.2
<br><br>

<?php 
$a = 5;
$b = 4;

echo "$a == $b : " . ($a == $b);
echo "<br> $a != $b : " . ($a !=  $b);
echo "<br> $a > $b : " . ($a > $b);
echo "<br> $a < $b : " . ($a < $b);
echo "<br> ($a == $b) && ($a > $b ) : " . (($a != $b) && ($a > $b ));
echo "<br> ($a == $b) || ($a < $b ) : " . (($a != $b) || ($a < $b )); 

?>