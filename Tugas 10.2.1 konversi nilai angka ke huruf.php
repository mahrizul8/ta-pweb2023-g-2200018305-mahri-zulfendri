<form action="">
    <input type="text" name="nilai">
    <button type="submit" name="submit">Kirim</button>
</form>

<?php 
if(isset($_GET['submit']))
{
    $nilai = $_GET['nilai'];
    
    if($nilai >= 80.00 && $nilai <= 100.00)
    {
        echo "nilai $nilai : A";

    } else if($nilai >= 76.25 && $nilai <= 79.99)
    {
        echo "nilai $nilai : A-";
        
    } else if($nilai >=  68.75 && $nilai <= 76.25)
    {
        echo "nilai $nilai : B+";
        
    } else if($nilai >=  65.00 && $nilai <= 68.74)
    {
        echo "nilai $nilai : B";
    
    } else if($nilai >=  62.50 && $nilai <= 64.99)
    {
        echo "nilai $nilai : B-";
    
    } else if($nilai >=  57.50 && $nilai <= 62.49)
    {
        echo "nilai $nilai : C+";

    } else if($nilai >=  55.00 && $nilai <= 57.49)
    {
        echo "nilai $nilai : C";

    } else if($nilai >=  51.25 && $nilai <= 54.99)
    {
        echo "nilai $nilai : C-";

    } else if($nilai >=  43.75 && $nilai <= 51.24)
    {
        echo "nilai $nilai : D+";

    } else if($nilai >=  40.00 && $nilai <= 43.74)
    {
        echo "nilai $nilai : D";

    } else if($nilai >=  0.00 && $nilai <= 39.99)
    {
        echo "nilai $nilai : E";
    }
}