<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <p>Menggunakan JavaScript</p>
    <script>
        var baris, i, j = 0;
        var nilai = prompt('tinggi: ', 5);
        var tinggi = parseInt(nilai);

        for(baris = 1; baris <= tinggi; baris++)
        {
            for(i = 1; i <= tinggi - baris; i++)
            {
                document.write('&nbsp;&nbsp;');
            }
            for(j = 1; j < 2 * baris; j++)
            {
                document.write("*");
            }
            document.write("<br>\n");
        }
    </script>
    <br>
    <p>Menggunakan PHP</p>
    <?php 
    $baris = 0;
    $i = 0;
    $j = 0;

    $tinggi = 5;
    for($baris = 1; $baris <= $tinggi; $baris++)
    {
        for($i = 1; $i <= $tinggi - $baris; $i++)
        {
            echo "&nbsp;&nbsp;";
        }
        for($j = 1; $j < 2 * $baris; $j++)
        {
            echo "*";
        }
        echo "<br>\n";
    }
    ?>
</body>
</html>