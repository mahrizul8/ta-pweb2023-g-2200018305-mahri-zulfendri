<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="styleFormValidationJS.css">
</head>
<body>
    <center>
        Portal UAD
    </center>
    <div class="login">
        <form action="#" method="post" onsubmit="validasi()">
            <div>
                <label for="nama">Nama Lengkap:</label>
                <input type="text" name="nama" id="nama">
            </div>
            <div>
                <label for="email">Email: </label>
                <input type="email" name="email" id="email">
            </div>
            <div>
                <label for="alamat">Alamat: </label>
                <textarea name="alamat" id="alamat" cols="30" rows="10"></textarea>
            </div>
            <div>
                <input type="submit" value="Daftar" class="tombol">
            </div>
        </form>
    </div>
    <script>
        function validasi()
        {
            var nama = document.getElementById("nama").value;
            var email = document.getElementById("email").value;
            var alamat = document.getElementById("alamat").value;

            if(nama != "" && email != "" && alamat != "")
            {
                return true;
            }
            else 
            {
                alert("Anda harus mengisi data dengan lengkap");
            }
        }
    </script>
</body>
</html>