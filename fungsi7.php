<?php

function luas_lingkaran($jari)
{
    return 3.14 * $jari * $jari;
}

$arr = get_defined_functions();
echo "<pre>";

if(in_array("exif_read_data", $arr))
{
    echo "Fungsi exif_read_data di PHP versi ini";
} else {
    echo "Fungsi exif_read_data tidak ada di PHP versi ini";
}
echo "</pre>";